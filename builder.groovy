def choice=[]
node('devops') {
    choice = params["MODES"].split(",")
}

// Check if there is a job in Security Infusion folder in Jenkins otherwise check the infusion images repo and build the dockerfile.
def build_image(image_name,pipeline,dockerfile,tag,context){
    script{
        if ( pipeline != null) {
            build job  : "/SecurityInfusion/$pipeline", parameters: [[ $class: 'StringParameterValue' ,name:"TAG", value: "$tag" ]]
        } else {
            script{
                docker.withRegistry('https://harbor.devops2.itml.tech', 'harbor-jenkins-user') {
                    def customImage = docker.build("harbor.devops2.itml.tech/securityinfusion/$image_name"+"-base:"+"$tag","-f $dockerfile $context")
                    customImage.push()
                }
            }
        }
    }
}

def build_csm_image(image_name,pipeline,dockerfile,tag,context){
    script{
        if ( pipeline != null) {
            build job  : "/SecurityInfusion/$pipeline", parameters: [[$class: 'StringParameterValue', name: 'LinuxAgentVersion', value: "$LinuxAgentVersion"],[ $class: 'StringParameterValue' ,name:"MacAgentVersion", value: "$MacAgentVersion"],[ $class: 'StringParameterValue' ,name:"WindowsInstallerVersion", value: "$WindowsInstallerVersion"],[ $class: 'StringParameterValue' ,name:"TAG", value: "$tag"]]
        } else {
            script{
                docker.withRegistry('https://harbor.devops2.itml.tech', 'harbor-jenkins-user') {
                    def customImage = docker.build("harbor.devops2.itml.tech/securityinfusion/$image_name"+"-base:"+"$tag","-f $dockerfile $context")
                    customImage.push()
                }
            }
        }
    }
}

def pull_image(image_name,image_tag){
    script{
        if(image_name == "csm_app") {
            docker.withRegistry('https://harbor.devops2.itml.tech', 'harbor-jenkins-user') {
                sh """
                    docker pull harbor.devops2.itml.tech/securityinfusion/$image_name:$image_tag
                    docker tag harbor.devops2.itml.tech/securityinfusion/$image_name:$image_tag $image_name-base:$image_tag
                    docker save -o '$image_name'.tar $image_name-base:$image_tag 
                """
            }
        } else {
            docker.withRegistry('https://harbor.devops2.itml.tech', 'harbor-jenkins-user') {
                sh """
                    docker pull harbor.devops2.itml.tech/securityinfusion/$image_name-base:$image_tag
                    docker tag harbor.devops2.itml.tech/securityinfusion/$image_name-base:$image_tag $image_name-base:$image_tag
                    docker save -o '$image_name'.tar $image_name-base:$image_tag 
                """
            }
        }
    }
}

pipeline {

    agent { label 'devops'}

    parameters {
        checkboxParameter(name: 'MODES', format: 'JSON', pipelineSubmitContent: '{"CheckboxParameter": [{"key": "Import image to Harbor","value": "importHarbor"},{"key": "Extract images from Harbor","value": "extractHarbor"}]}', description: 'Choose the modes in which the pipeline will run.')
        listGitBranches( branchFilter: 'origin/master', name: 'backupper_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/backupper.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'connector_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/secinfusion-es-connector.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'csm_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.1.3240b0d', sortMode: 'DESCENDING', remoteURL: 'https://bitbucket.org/itmlgr/infusion-csm.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'notifier_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/notifier.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'dfbAdmin_TAG', type: 'PT_TAG',  defaultValue: 'v1.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/dfb-admin.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'filebeat_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/infusion-images.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'nagios_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/infusion-images.git', credentialsId: 'bitbucket_password') 
        listGitBranches( branchFilter: 'origin/master', name: 'ossec_TAG', type: 'PT_TAG',  defaultValue: 'v2.8.0', sortMode: 'DESCENDING_SMART', remoteURL: 'https://bitbucket.org/itmlgr/infusion-images.git', credentialsId: 'bitbucket_password') 
        string(name: "Installation_TAG", defaultValue: "", description: "Tag to be attached to all images pushed to Harbor." )
        string(name: "LinuxAgentVersion", defaultValue: "", description: "Version of the Linux agent." )
        string(name: "MacAgentVersion", defaultValue: "", description: "Version of the Mac agent." )
        string(name: "WindowsInstallerVersion", defaultValue: "", description: "Version of the Windows installer." )
    }

    stages{
        
        stage("Import images in Harbor"){

            when { expression { 'importHarbor' in choice } }

            stages{

                stage("Checkout"){
                    steps{
                        checkout([$class: 'GitSCM',
                            branches: [[name: 'refs/tags/"$filebeat_TAG"']],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [],
                            gitTool: 'Default',
                            submoduleCfg: [],
                            userRemoteConfigs: [[url: 'https://bitbucket.org/itmlgr/infusion-images.git', credentialsId: 'bitbucket_password']]
                        ])
                        sh '''
                            pwd
                            ls -la
                        '''
                    }
                }
                
                stage("Build images"){
                
                    parallel {

                        stage("Build Backupper image"){
                            steps{
                                build_image("backupper-base","secinf-backupper-build-pipeline",null,"$backupper_TAG",null)
                            }
                        }
                        stage("Build Csm image"){
                            steps{
                                build_csm_image("csm",'secinf-csm-build-pipeline',null,"$csm_TAG",null)
                            }
                        }
                        stage("Build DfbAdmin image"){
                            steps{
                                build_image("dfbadmin",'secinf-dfb-admin-build-pipeline',null,"$dfbAdmin_TAG",null)
                            }
                        }
                        stage("Build Es-connector image"){
                            steps{
                                build_image("es-connector",'secinf-connector-build-pipeline',null,"$connector_TAG",null)
                            }
                        }
                        stage("Build Notifier image"){
                            steps{
                                build_image("notifier",'secinf-notifier-build-pipeline',null,"$notifier_TAG",null)
                            }
                        }
                        stage("Build Filebeat image"){
                            steps{
                                build_image("filebeat",null,'./filebeat/filebeat_dockerfile',"$filebeat_TAG",'./filebeat')
                            }
                        }
                        stage("Build Nagios image"){
                            steps{
                                build_image("nagios",null,'./nagios/nagios_dockerfile',"$nagios_TAG",'./nagios')
                            }
                        }
                        stage("Build Ossec image"){
                            steps{
                                build_image("ossec",null,'./ossec/ossec_dockerfile',"$ossec_TAG",'./ossec')
                            }
                        }

                    }

                }

                stage("Delete directories"){
                    steps{
                        cleanWs deleteDirs: true
                    }
                }

            }
        }

        stage("Extract images from Harbor"){

            when { expression { 'extractHarbor' in choice } }

            stages{

                stage("Create directory"){
                    steps{
                        script{
                            sh '''
                                mkdir infusion-images 
                            '''
                        }
                    }
                }

                stage("Create parameters.txt"){
                    steps{
                        dir('infusion-images'){
                            script{
                                sh """
                                    touch info.txt
                                    echo "backupper tag: ${backupper_TAG}" >> info.txt
                                    echo "es-connector tag: ${connector_TAG}" >> info.txt
                                    echo "csm tag: ${csm_TAG}" >> info.txt
                                    echo "notifier tag: ${notifier_TAG}" >> info.txt
                                    echo "dfb-Admin tag: ${dfbAdmin_TAG}" >> info.txt
                                    echo "filebeat tag: ${filebeat_TAG}" >> info.txt
                                    echo "nagios tag: ${nagios_TAG}" >> info.txt
                                    echo "ossec tag: ${ossec_TAG}" >> info.txt
                                    echo "installation tag: ${Installation_TAG}" >> info.txt
                                    echo "Linux Agent artifact name: ${LinuxAgentVersion}" >> info.txt
                                    echo "Mac Agent artifact name: ${MacAgentVersion}" >> info.txt
                                    echo "Windows Installer artifact name: ${WindowsInstallerVersion}" >> info.txt
                                """
                            }
                        }
                    }
                }

                stage("Pull images"){

                    parallel {

                        stage("Pull Backupper image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("backuper","$backupper_TAG")
                                }
                            }
                        }
                        stage("Pull Csm image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("csm_app","$csm_TAG")
                                    pull_image("csm-encrypted","$csm_TAG")
                                }
                            }
                        }
                        stage("Pull DfbAdmin image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("dfb-admin","$dfbAdmin_TAG")
                                }
                            }
                        }
                        stage("Pull Es-connector image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("es-connector","$connector_TAG")
                                }
                            }
                        }
                        stage("Pull Notifier image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("notifier","$notifier_TAG")
                                }
                            }
                        }
                        stage("Pull Filebeat image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("filebeat","$filebeat_TAG")
                                }
                            }
                        }
                        stage("Pull Nagios image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("nagios","$nagios_TAG")
                                }
                            }
                        }
                        stage("Pull Ossec image"){
                            steps{
                                dir('infusion-images'){
                                    pull_image("ossec","$ossec_TAG")
                                }
                            }
                        }

                    }
                }
                
                stage("Create .tar.gz file"){
                    steps{
                        script{
                            sh """ 
                                tar -zcf infusion-images-'$Installation_TAG'.tar.gz infusion-images
                                mkdir /home/jenkins/infusion-images
                                mv infusion-images.tar.gz /home/jenkins/infusion-images
                            """
                        }
                    }
                }

            }            
            
        }
    }

    post {
        always{
            cleanWs deleteDirs: true
        }
        success {
            echo "The pipeline finished successfully!"
        }
    }

}